#!/bin/sh
# Start/stop/restart mariadbd.
#
# Copyright 2003  Patrick J. Volkerding, Concord, CA
# Copyright 2003  Slackware Linux, Inc., Concord, CA
# Copyright 2008, 2013, 2024  Patrick J. Volkerding, Sebeka, MN, USA
#
# This program comes with NO WARRANTY, to the extent permitted by law.
# You may redistribute copies of this program under the terms of the
# GNU General Public License.

# To start MariaDB automatically at boot, be sure this script is executable:
# chmod 755 /etc/rc.d/rc.mysqld

# Before you can run MariaDB, you must have a database.  To install an initial
# database, do this as root:
#
#   mariadb-install-db --user=mysql
#
# Note that the mysql user must exist in /etc/passwd, and the created files
# will be owned by this dedicated user.  This is important, or else mariadb
# (which runs as user "mysql") will not be able to write to the database
# later (this can be fixed with 'chown -R mysql.mysql /var/lib/mysql').
#
# To increase system security, consider using "mariadb-secure-installation"
# as well.  For more information on this tool, please read:
#   man mariadb-secure-installation

# To set additional options (or see defaults), see:
#   /etc/default/mariadb

# Source /etc/default/mariadb to set $MARIADB_OPTIONS:
if [ -r /etc/default/mariadb ]; then
  . /etc/default/mariadb
fi

# Start mariadbd:
mariadbd_start() {
  if [ -x /usr/bin/mariadbd-safe ]; then
    # If there is an old PID file (no mysqld running), clean it up:
    if [ -r /var/run/mysql/mysql.pid ]; then
      if ! ps axc | grep mysqld 1> /dev/null 2> /dev/null ; then
        echo "Cleaning up old /var/run/mysql/mysql.pid."
        rm -f /var/run/mysql/mysql.pid
      fi
    fi

    /usr/bin/mariadbd-safe --datadir=/var/lib/mysql --pid-file=/var/run/mysql/mysql.pid $MARIADB_OPTIONS &
  fi
}

# Stop mariadbd:
mariadbd_stop() {
  # If there is no PID file, ignore this request...
  if [ -r /var/run/mysql/mysql.pid ]; then
    PID=$(cat /var/run/mysql/mysql.pid)
    kill $PID
    # Wait at least one minute for it to exit, as we don't know how big the DB is...
    for second in $(seq 0 60) ; do
      if [ ! -r /var/run/mysql/mysql.pid ]; then
        break;
      fi
      sleep 1
    done
    if [ "$second" = "60" ]; then
      echo "WARNING:  Gave up waiting for mariadbd to exit!"
      sleep 15
    fi
  fi
}

# Restart mariadbd:
mariadbd_restart() {
  mariadbd_stop
  mariadbd_start
}

case "$1" in
'start')
  mariadbd_start
  ;;
'stop')
  mariadbd_stop
  ;;
'restart')
  mariadbd_restart
  ;;
*)
  echo "usage $0 start|stop|restart"
esac
