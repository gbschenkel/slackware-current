#!/bin/sh
# Choose a default for the system's manual pager:
#export MANPAGER=less
#export MANPAGER=more
#export MANPAGER=most

# This fixes color output with some pagers:
export MANROFFOPT=-c
