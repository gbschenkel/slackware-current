#!/bin/csh
# Choose a default for the system's manual pager:
#setenv MANPAGER less
#setenv MANPAGER more
#setenv MANPAGER most

# This fixes color output with some pagers:
setenv MANROFFOPT -c
