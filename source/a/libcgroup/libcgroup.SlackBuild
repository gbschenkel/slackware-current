#!/bin/bash

# Copyright 2011, 2014, 2017, 2018, 2021, 2025  Patrick J. Volkerding, Sebeka, Minnesota, USA
# All rights reserved.
#
# Redistribution and use of this script, with or without modification, is
# permitted provided that the following conditions are met:
#
# 1. Redistributions of this script must retain the above copyright
#    notice, this list of conditions and the following disclaimer.
#
#  THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR IMPLIED
#  WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
#  MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.  IN NO
#  EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
#  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
#  PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
#  OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
#  WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
#  OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
#  ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

cd $(dirname $0) ; CWD=$(pwd)

PKGNAM=libcgroup
VERSION=${VERSION:-$(echo $PKGNAM-*.tar.?z | rev | cut -f 3- -d . | cut -f 1 -d - | rev)}
BUILD=${BUILD:-3}

# Automatically determine the architecture we're building on:
if [ -z "$ARCH" ]; then
  case "$(uname -m)" in
    i?86) ARCH=i686 ;;
    arm*) readelf /usr/bin/file -A | egrep -q "Tag_CPU.*[4,5]" && ARCH=arm || ARCH=armv7hl ;;
    # Unless $ARCH is already set, use uname -m for all other archs:
    *) ARCH=$(uname -m) ;;
  esac
  export ARCH
fi

# If the variable PRINT_PACKAGE_NAME is set, then this script will report what
# the name of the created package would be, and then exit. This information
# could be useful to other scripts.
if [ ! -z "${PRINT_PACKAGE_NAME}" ]; then
  echo "$PKGNAM-$VERSION-$ARCH-$BUILD.txz"
  exit 0
fi

NUMJOBS=${NUMJOBS:-" -j $(expr $(nproc) + 1) "}

if [ "$ARCH" = "i686" ]; then
  SLKCFLAGS="-O2 -march=pentium4 -mtune=generic"
  LIBDIRSUFFIX=""
elif [ "$ARCH" = "x86_64" ]; then
  SLKCFLAGS="-O2 -march=x86-64 -mtune=generic -fPIC"
  LIBDIRSUFFIX="64"
else
  SLKCFLAGS="-O2"
  LIBDIRSUFFIX=""
fi

TMP=${TMP:-/tmp}
PKG=$TMP/package-$PKGNAM

rm -rf $PKG
mkdir -p $TMP $PKG

cd $TMP
rm -rf $PKGNAM-$VERSION
tar xvf $CWD/$PKGNAM-$VERSION.tar.?z || exit 1
cd $PKGNAM-$VERSION || exit 1

chown -R root:root .
find . \
  \( -perm 777 -o -perm 775 -o -perm 711 -o -perm 555 -o -perm 511 \) \
  -exec chmod 755 {} \+ -o \
  \( -perm 666 -o -perm 664 -o -perm 600 -o -perm 444 -o -perm 440 -o -perm 400 \) \
  -exec chmod 644 {} \+

# Label cgred.conf properly in the first line:
cat $CWD/cgred.conf.no.sysconfig.patch | patch -p1 --verbose || exit 1

# Configure, build, and install:
if [ ! -r configure ]; then
  if [ -x ./autogen.sh ]; then
    NOCONFIGURE=1 ./autogen.sh
  else
    autoreconf -vif
  fi
fi
CFLAGS="$SLKCFLAGS" \
CXXFLAGS="$SLKCFLAGS" \
./configure \
  --prefix=/usr \
  --libdir=/usr/lib${LIBDIRSUFFIX} \
  --bindir=/usr/bin \
  --sbindir=/usr/sbin \
  --sysconfdir=/etc \
  --localstatedir=/var \
  --docdir=/usr/doc/$PKGNAM-$VERSION \
  --mandir=/usr/man \
  --infodir=/usr/info \
  --disable-static \
  --enable-pam \
  --enable-pam-module-dir=/lib${LIBDIRSUFFIX}/security \
  --enable-systemd=no \
  --build=$ARCH-slackware-linux || exit 1
make $NUMJOBS || make || exit 1
make install DESTDIR=$PKG || exit 1

# Even though we do not use systemd, breakage results if this header file
# does not exist. And the PRs to remove it from libcgroup.h cause this package
# to not even build.
#
# As a first attempt, let's just make it an empty file to see if that fixes
# downstream issues:
if [ ! -e $PKG/usr/include/libcgroup/systemd.h ]; then
  touch $PKG/usr/include/libcgroup/systemd.h
fi

# This directory is needed by cgrulesengd:
mkdir -p $PKG/etc/cgconfig.d

# cgexec doesn't need to be setuid root -- setgid cgred is sufficient:
chown root:cgred $PKG/usr/bin/cgexec
chmod 2755 $PKG/usr/bin/cgexec

# Install init scripts. These now test for things like LSB and sysconfig and
# don't try to use them if they don't exist. Therefore we don't need to patch
# these for the reasons we did before. However, we might need patches to
# properly support cgroups v2. But first things first...
mkdir -p $PKG/etc/rc.d
cp -a scripts/init.d/cgconfig $PKG/etc/rc.d/rc.cgconfig.new
cp -a scripts/init.d/cgred $PKG/etc/rc.d/rc.cgred.new
chmod 644 $PKG/etc/rc.d/*
chown -R root:root $PKG/etc/rc.d/*

# Install sample config files:
( cd samples/config
  for conffile in cgconfig.conf cgred.conf cgrules.conf cgsnapshot_allowlist.conf cgsnapshot_denylist.conf ; do
    cp -a $conffile $PKG/etc/${conffile}.new
  done
)

# Add a default file to signal to rc.S which cgroups hierarchy to mount:
mkdir -p $PKG/etc/default
cat $CWD/cgroups.default > $PKG/etc/default/cgroups.new

# Don't ship .la files:
rm -f $PKG/{,usr/}lib${LIBDIRSUFFIX}/*.la

# Strip binaries:
find $PKG | xargs file | grep -e "executable" -e "shared object" | grep ELF | cut -f 1 -d : | xargs strip --strip-unneeded 2> /dev/null

# Compress manual pages:
find $PKG/usr/man -type f -exec gzip -9 {} \+
for i in $( find $PKG/usr/man -type l ) ; do
  ln -s $( readlink $i ).gz $i.gz
  rm $i
done

# Add a documentation directory:
mkdir -p $PKG/usr/doc/${PKGNAM}-$VERSION
cp -a \
  CONTRIBUTING* COPYING* INSTALL* README* SECURITY* \
  samples \
  $PKG/usr/doc/${PKGNAM}-$VERSION
find $PKG/usr/doc/${PKGNAM}-$VERSION -name "Makefile*" | xargs rm
find $PKG/usr/doc/${PKGNAM}-$VERSION -name ".deps" | xargs rm -r

mkdir -p $PKG/install
cat $CWD/doinst.sh > $PKG/install/doinst.sh
cat $CWD/slack-desc > $PKG/install/slack-desc

cd $PKG
/sbin/makepkg -l y -c n $TMP/$PKGNAM-$VERSION-$ARCH-$BUILD.txz
